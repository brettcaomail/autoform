<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'activated' => true,
        'created_at' => $faker->dateTime,
        'deleted_at' => null,
        'email' => $faker->email,
        'first_name' => $faker->firstName,
        'forbidden' => $faker->boolean(),
        'language' => 'en',
        'last_login_at' => $faker->dateTime,
        'last_name' => $faker->lastName,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'updated_at' => $faker->dateTime,
        
    ];
});/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\STPPTNR::class, static function (Faker\Generator $faker) {
    return [
        'STCD' => $faker->sentence,
        'TM' => $faker->dateTime,
        'DRP' => $faker->randomNumber(5),
        'INTV' => $faker->randomNumber(5),
        'PDR' => $faker->randomNumber(5),
        'DYP' => $faker->randomNumber(5),
        'WTH' => $faker->sentence,
        'ENTERDATE' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Test::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'task' => $faker->sentence,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Test::class, static function (Faker\Generator $faker) {
    return [
        'created_at' => $faker->dateTime,
        'name' => $faker->firstName,
        'task' => $faker->sentence,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Test::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'task' => $faker->sentence,
        'time' => $faker->dateTime,
        
        
    ];
});
