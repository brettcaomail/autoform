<?php

namespace App\Http\Requests\Admin\STPPTNR;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreSTPPTNR extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.s-t-p-p-t-n-r.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'STCD' => ['required', 'string'],
            'TM' => ['required', 'date'],
            'DRP' => ['nullable', 'numeric'],
            'INTV' => ['nullable', 'numeric'],
            'PDR' => ['nullable', 'numeric'],
            'DYP' => ['nullable', 'numeric'],
            'WTH' => ['nullable', 'string'],
            'ENTERDATE' => ['nullable', 'date'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
