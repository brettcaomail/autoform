<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('admin-users')->name('admin-users/')->group(static function() {
            Route::get('/',                                             'AdminUsersController@index')->name('index');
            Route::get('/create',                                       'AdminUsersController@create')->name('create');
            Route::post('/',                                            'AdminUsersController@store')->name('store');
            Route::get('/{adminUser}/impersonal-login',                 'AdminUsersController@impersonalLogin')->name('impersonal-login');
            Route::get('/{adminUser}/edit',                             'AdminUsersController@edit')->name('edit');
            Route::post('/{adminUser}',                                 'AdminUsersController@update')->name('update');
            Route::delete('/{adminUser}',                               'AdminUsersController@destroy')->name('destroy');
            Route::get('/{adminUser}/resend-activation',                'AdminUsersController@resendActivationEmail')->name('resendActivationEmail');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::get('/profile',                                      'ProfileController@editProfile')->name('edit-profile');
        Route::post('/profile',                                     'ProfileController@updateProfile')->name('update-profile');
        Route::get('/password',                                     'ProfileController@editPassword')->name('edit-password');
        Route::post('/password',                                    'ProfileController@updatePassword')->name('update-password');
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('s-t-p-p-t-n-r-s')->name('s-t-p-p-t-n-r-s/')->group(static function() {
            Route::get('/',                                             'STPPTNRController@index')->name('index');
            Route::get('/create',                                       'STPPTNRController@create')->name('create');
            Route::post('/',                                            'STPPTNRController@store')->name('store');
            Route::get('/{sTPPTNR}/edit',                               'STPPTNRController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'STPPTNRController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{sTPPTNR}',                                   'STPPTNRController@update')->name('update');
            Route::delete('/{sTPPTNR}',                                 'STPPTNRController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('tests')->name('tests/')->group(static function() {
            Route::get('/',                                             'TestController@index')->name('index');
            Route::get('/create',                                       'TestController@create')->name('create');
            Route::post('/',                                            'TestController@store')->name('store');
            Route::get('/{test}/edit',                                  'TestController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'TestController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{test}',                                      'TestController@update')->name('update');
            Route::delete('/{test}',                                    'TestController@destroy')->name('destroy');
        });
    });
});