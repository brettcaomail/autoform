import AppForm from '../app-components/Form/AppForm';

Vue.component('s-t-p-p-t-n-r-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                STCD:  '' ,
                TM:  '' ,
                DRP:  '' ,
                INTV:  '' ,
                PDR:  '' ,
                DYP:  '' ,
                WTH:  '' ,
                ENTERDATE:  '' ,
                
            }
        }
    }

});