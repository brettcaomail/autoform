@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.s-t-p-p-t-n-r.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">
        
        <s-t-p-p-t-n-r-form
            :action="'{{ url('admin/s-t-p-p-t-n-r-s') }}'"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.s-t-p-p-t-n-r.actions.create') }}
                </div>

                <div class="card-body">
                    @include('admin.s-t-p-p-t-n-r.components.form-elements')
                </div>
                                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                
            </form>

        </s-t-p-p-t-n-r-form>

        </div>

        </div>

    
@endsection