@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.s-t-p-p-t-n-r.actions.edit', ['name' => $sTPPTNR->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <s-t-p-p-t-n-r-form
                :action="'{{ $sTPPTNR->resource_url }}'"
                :data="{{ $sTPPTNR->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.s-t-p-p-t-n-r.actions.edit', ['name' => $sTPPTNR->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.s-t-p-p-t-n-r.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </s-t-p-p-t-n-r-form>

        </div>
    
</div>

@endsection