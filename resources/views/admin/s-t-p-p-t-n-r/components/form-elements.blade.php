<div class="form-group row align-items-center" :class="{'has-danger': errors.has('STCD'), 'has-success': fields.STCD && fields.STCD.valid }">
    <label for="STCD" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.s-t-p-p-t-n-r.columns.STCD') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.STCD" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('STCD'), 'form-control-success': fields.STCD && fields.STCD.valid}" id="STCD" name="STCD" placeholder="{{ trans('admin.s-t-p-p-t-n-r.columns.STCD') }}">
        <div v-if="errors.has('STCD')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('STCD') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('TM'), 'has-success': fields.TM && fields.TM.valid }">
    <label for="TM" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.s-t-p-p-t-n-r.columns.TM') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.TM" :config="datetimePickerConfig" v-validate="'required|date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('TM'), 'form-control-success': fields.TM && fields.TM.valid}" id="TM" name="TM" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('TM')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('TM') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('DRP'), 'has-success': fields.DRP && fields.DRP.valid }">
    <label for="DRP" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.s-t-p-p-t-n-r.columns.DRP') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.DRP" v-validate="'decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('DRP'), 'form-control-success': fields.DRP && fields.DRP.valid}" id="DRP" name="DRP" placeholder="{{ trans('admin.s-t-p-p-t-n-r.columns.DRP') }}">
        <div v-if="errors.has('DRP')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('DRP') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('INTV'), 'has-success': fields.INTV && fields.INTV.valid }">
    <label for="INTV" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.s-t-p-p-t-n-r.columns.INTV') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.INTV" v-validate="'decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('INTV'), 'form-control-success': fields.INTV && fields.INTV.valid}" id="INTV" name="INTV" placeholder="{{ trans('admin.s-t-p-p-t-n-r.columns.INTV') }}">
        <div v-if="errors.has('INTV')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('INTV') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('PDR'), 'has-success': fields.PDR && fields.PDR.valid }">
    <label for="PDR" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.s-t-p-p-t-n-r.columns.PDR') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.PDR" v-validate="'decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('PDR'), 'form-control-success': fields.PDR && fields.PDR.valid}" id="PDR" name="PDR" placeholder="{{ trans('admin.s-t-p-p-t-n-r.columns.PDR') }}">
        <div v-if="errors.has('PDR')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('PDR') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('DYP'), 'has-success': fields.DYP && fields.DYP.valid }">
    <label for="DYP" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.s-t-p-p-t-n-r.columns.DYP') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.DYP" v-validate="'decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('DYP'), 'form-control-success': fields.DYP && fields.DYP.valid}" id="DYP" name="DYP" placeholder="{{ trans('admin.s-t-p-p-t-n-r.columns.DYP') }}">
        <div v-if="errors.has('DYP')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('DYP') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('WTH'), 'has-success': fields.WTH && fields.WTH.valid }">
    <label for="WTH" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.s-t-p-p-t-n-r.columns.WTH') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.WTH" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('WTH'), 'form-control-success': fields.WTH && fields.WTH.valid}" id="WTH" name="WTH" placeholder="{{ trans('admin.s-t-p-p-t-n-r.columns.WTH') }}">
        <div v-if="errors.has('WTH')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('WTH') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('ENTERDATE'), 'has-success': fields.ENTERDATE && fields.ENTERDATE.valid }">
    <label for="ENTERDATE" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.s-t-p-p-t-n-r.columns.ENTERDATE') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.ENTERDATE" :config="datetimePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('ENTERDATE'), 'form-control-success': fields.ENTERDATE && fields.ENTERDATE.valid}" id="ENTERDATE" name="ENTERDATE" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('ENTERDATE')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('ENTERDATE') }}</div>
    </div>
</div>


