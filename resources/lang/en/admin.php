<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'activated' => 'Activated',
            'email' => 'Email',
            'first_name' => 'First name',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
            'last_name' => 'Last name',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',

            //Belongs to many relations
            'roles' => 'Roles',

        ],
    ],

    's-t-p-p-t-n-r' => [
        'title' => 'St Pptn R',

        'actions' => [
            'index' => 'St Pptn R',
            'create' => 'New St Pptn R',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'STCD' => 'STCD',
            'TM' => 'TM',
            'DRP' => 'DRP',
            'INTV' => 'INTV',
            'PDR' => 'PDR',
            'DYP' => 'DYP',
            'WTH' => 'WTH',
            'ENTERDATE' => 'ENTERDATE',

        ],
    ],

    's-t-s-t-b-p-r-p-b' => [
        'title' => 'St Stbprp B',

        'actions' => [
            'index' => 'St Stbprp B',
            'create' => 'New St Stbprp B',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'STCD' => 'STCD',
            'STNM' => 'STNM',
            'RVNM' => 'RVNM',
            'HNNM' => 'HNNM',
            'BSNM' => 'BSNM',
            'STLC' => 'STLC',
            'ADDVCD' => 'ADDVCD',
            'MDBZ' => 'MDBZ',
            'MDPR' => 'MDPR',
            'DTMNM' => 'DTMNM',
            'DTMEL' => 'DTMEL',
            'STTP' => 'STTP',
            'DFRTMS' => 'DFRTMS',
            'FRITM' => 'FRITM',
            'FRGRD' => 'FRGRD',
            'BGFRYM' => 'BGFRYM',
            'EDFRYM' => 'EDFRYM',
            'ADMAUTH' => 'ADMAUTH',
            'STBK' => 'STBK',
            'DRNA' => 'DRNA',
            'PHCD' => 'PHCD',
            'LGTD' => 'LGTD',
            'LTTD' => 'LTTD',
            'DTPR' => 'DTPR',
            'ESSTYM' => 'ESSTYM',
            'ATCUNIT' => 'ATCUNIT',
            'LOCALITY' => 'LOCALITY',
            'STAZT' => 'STAZT',
            'DSTRVM' => 'DSTRVM',
            'USFL' => 'USFL',
            'COMMENTS' => 'COMMENTS',
            'MODITIME' => 'MODITIME',
            'bscd' => 'Bscd',
            'branch' => 'Branch',
            'county' => 'County',
            'countyside' => 'Countyside',
            'showx' => 'Showx',
            'showy' => 'Showy',
            'showyn' => 'Showyn',
            'printx' => 'Printx',
            'printy' => 'Printy',
            'printyn' => 'Printyn',
            'stats' => 'Stats',
            'dr' => 'Dr',
            'p' => 'P',
            'h' => 'H',
            'k' => 'K',
            'm' => 'M',
            'g' => 'G',
            'fp' => 'Fp',
            'fh' => 'Fh',
            'fk' => 'Fk',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];
